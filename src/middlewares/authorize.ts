import express from 'express'
import TokenServices from '~/services/tokenServices'
import JWT from '~/utils/jwt'

async function isAuthorized(req: express.Request, res: express.Response, next: express.NextFunction) {
    const fullAccessToken = req.body.accessToken
    const fullRefreshToken = req.body.refreshToken

    try {
        const validateATResult: any = JWT.validateJWT(fullAccessToken, 'access')
        if (validateATResult instanceof Error) {
            if (validateATResult.name === 'TokenExpiredError') {
                const validateRTResult: any = JWT.validateJWT(fullRefreshToken, 'refresh')
                if (validateRTResult instanceof Error) {
                    if (validateRTResult.name === 'TokenExpiredError')
                        res.status(401).json({ message: 'Both token expired, required Login', isAuthorized: false })
                } else {
                    const response = await TokenServices.deleteTokenByUserId(validateRTResult.userId)
                    if (!response.error) {
                        const newRefreshToken = JWT.generateJWT(
                            { userId: validateRTResult.userId, name: validateRTResult.name },
                            'refresh'
                        )
                        if (newRefreshToken) {
                            await TokenServices.createNewToken({
                                token: newRefreshToken,
                                user_id: validateRTResult.userId
                            })
                        }
                        const newAccessToken = JWT.generateJWT(
                            { userId: validateRTResult.userId, name: validateRTResult.name },
                            'access'
                        )
                        res.status(201).json({
                            newAccessToken: newAccessToken,
                            newRefreshToken: newRefreshToken,
                            isAuthorized: true,
                            message: 'Refresh successfully'
                        })
                    }
                }
            }
        } else {
            res.status(201).json({
                isAuthorized: true,
                message: 'Authorized successfully'
            })
        }
    } catch (error: any) {
        if (error) {
            console.log('Token expired')
        }
        if (!error.status) {
            error.status = 500
        }
        next(error)
    }
}

