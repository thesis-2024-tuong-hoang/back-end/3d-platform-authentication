import { Router } from 'express'
import AuthController from '~/controllers/authController'
import { checkSchema } from 'express-validator'
import ValidateSchemas from '~/utils/validate'
// import { isAuthorized } from '~/middlewares/authorize'

const router = Router()

router.post('/register', checkSchema(ValidateSchemas.register, ['body']), AuthController.register)

router.post('/login', checkSchema(ValidateSchemas.login, ['body']), AuthController.login)

router.post('/login-with-google', AuthController.loginWithGoogle)

router.post('/logout', AuthController.logout)

router.post('/authorize', AuthController.authorize)

export default router
