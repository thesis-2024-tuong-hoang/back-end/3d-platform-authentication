import express from 'express'
import logger from 'morgan'
import router from './routes'
import cookieParser from 'cookie-parser'
import errorHandler from './middlewares/error'

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/auth', router)

app.use(errorHandler)

console.log('Authentication running on localhost:3003')

app.listen(3003)
