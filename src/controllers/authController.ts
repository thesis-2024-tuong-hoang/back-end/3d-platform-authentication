import * as express from 'express'
import { IUserRegister, IUserLogin, ITokenDecodedPayload } from '~/type'
import UserServices from '~/services/userServices'
import TokenServices from '~/services/tokenServices'
import { validationResult } from 'express-validator'
import JWT from '~/utils/jwt'

class AuthController {
    static async register(req: express.Request, res: express.Response, next: express.NextFunction) {
        const registerInput = req.body as IUserRegister

        const errors = validationResult(req)
        try {
            const response = await UserServices.register(registerInput, errors)
            res.status(response.status).json({
                message: 'Creating a new user successfully'
            })
        } catch (error) {
            next(error)
        }
    }

    static async login(req: express.Request, res: express.Response, next: express.NextFunction) {
        const loginInput = req.body as IUserLogin
        const errors = validationResult(req)

        try {
            const response = await UserServices.login(loginInput, errors)
            res.status(response.status).json({
                accessToken: response.accessToken,
                refreshToken: response.refreshToken,
                userId: response.userId,
                message: 'Login successfully'
            })
        } catch (error) {
            next(error)
        }
    }

    static async loginWithGoogle(req: express.Request, res: express.Response, next: express.NextFunction) {
        const data = req.body as any

        // console.log(data)

        try {
            const response = await UserServices.loginWithGoogle(data)
            res.status(response.status).json({
                accessToken: response.accessToken,
                refreshToken: response.refreshToken,
                userId: response.userId,
                message: 'Login with Google successfully!'
            })
        } catch (error) {
            next(error)
        }
    }

    static async logout(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userId = req.body 

        console.log("Useer: ", userId.id)


        try {
            const token = await TokenServices.getTokenByUserId(userId.id)

            if (token.token) {
                const response = await TokenServices.deleteTokenByUserId(userId.id)
                if (!response.error) {
                    res.status(200).json({ message: 'Logout successfully !' })
                }
            }
        } catch (error: any) {
            if (!error.status) {
                error.status = 500
            }
            next(error)
        }
    }

    static async authorize(req: express.Request, res: express.Response, next: express.NextFunction) {
        const fullAccessToken = req.body.accessToken
        const fullRefreshToken = req.body.refreshToken
    
        try {
            const validateATResult: any = JWT.validateJWT(fullAccessToken, 'access')
            if (validateATResult instanceof Error) {
                if (validateATResult.name === 'TokenExpiredError') {
                    const validateRTResult: any = JWT.validateJWT(fullRefreshToken, 'refresh')
                    if (validateRTResult instanceof Error) {
                        if (validateRTResult.name === 'TokenExpiredError')
                            res.status(401).json({ message: 'Both token expired, required Login', isAuthorized: false })
                    } else {
                        const response = await TokenServices.deleteTokenByUserId(validateRTResult.userId)
                        if (!response.error) {
                            const newRefreshToken = JWT.generateJWT(
                                { userId: validateRTResult.userId, name: validateRTResult.name },
                                'refresh'
                            )
                            if (newRefreshToken) {
                                await TokenServices.createNewToken({
                                    token: newRefreshToken,
                                    user_id: validateRTResult.userId
                                })
                            }
                            const newAccessToken = JWT.generateJWT(
                                { userId: validateRTResult.userId, name: validateRTResult.name },
                                'access'
                            )
                            res.status(201).json({
                                newAccessToken: newAccessToken,
                                newRefreshToken: newRefreshToken,
                                isAuthorized: true,
                                message: 'Refresh successfully'
                            })
                        }
                    }
                }
            } else {
                res.status(201).json({
                    isAuthorized: true,
                    message: 'Authorized successfully'
                })
            }
        } catch (error: any) {
            if (error) {
                console.log('Token expired')
            }
            if (!error.status) {
                error.status = 500
            }
            next(error)
        }
    }
}

export default AuthController
