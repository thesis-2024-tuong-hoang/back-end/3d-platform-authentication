import jwt from 'jsonwebtoken'
import { IPayloadToken, ITokenDecodedPayload } from '~/type'

type TTokenType = 'access' | 'refresh'

class JWT {
    private static ACCESS_TOKEN = 'accessAuth3d'
    private static REFRESH_TOKEN = 'refreshAuth3d'

    static generateJWT(payload: IPayloadToken, token: TTokenType) {
        const secret = token == 'access' ? this.ACCESS_TOKEN : this.REFRESH_TOKEN
        const expired = token == 'access' ? '3s' : '180d'
        if (token) return jwt.sign(payload, secret, { expiresIn: expired })
    }

    static validateJWT(currentToken: string, token: TTokenType) {
        const secret = token == 'access' ? this.ACCESS_TOKEN : this.REFRESH_TOKEN
        return jwt.verify(currentToken, secret, (error, decoded) => {
            if (!error) {
                return decoded as ITokenDecodedPayload
            } else {
                return error
            }
        })
    }
}

export default JWT
